export interface ISearchResult {
  contactInfo: {firstName: string, lastName: string, userType: string, userName: string, recordType: string, email: string, phone: string, info3AccountStatus: string, dob: string, id: string};
  vehicleProfiles: [{ yearMakeModel: string, vehicleProfile: string, vehicleName: string, associationStatus: string, removeRequestDate: string, vin: string }];
  applications: [{ contactApplicationName: string, applicationName: string, category: string }];
  agreements: [{ agreementRefreshDate: string, agreementsAccepted: string, status: string, lastAcceptDate: string }];
  favorites: [{ favoriteName: string, recordType: string, value: string }];
}



// IAccount = {
//   "contactInfo" : [{"firstName" : "Joseph"}, {"lastName" : "Kanaan"}],
//   "vehicleProfiles" : [{ "yearMakeModel":"2018 Chevrolet Corvette", "vehicleProfile" : "Joseph's 2018 Corvette 1GNKVHKD9HJ302182", "vehicleName" : "1GNKVHKD9HJ302182", "associationStatus" : "Active", "removeRequestDate" : "1/1/2018" },
//     { "yearMakeModel":"2018 Chevrolet Camaro", "vehicleProfile" : "Joseph's 2018 Camaro 1GNKVHKD9HJ302182", "vehicleName" : "1GNKVHKD9HJ302182", "associationStatus" : "Active", "removeRequestDate" : "1/2/2018" },
//     { "yearMakeModel":"2018 Cadillac CTSV", "vehicleProfile" : "Joseph's 2018 CTSV 1GNKVHKD9HJ302182", "vehicleName" : "1GNKVHKD9HJ302182", "associationStatus" : "Active", "removeRequestDate" : "1/3/2018" }]
// }
