import {AfterViewInit, Component, OnInit} from '@angular/core';
// import { IAccount } from './account'
// import { IAccount } from '../search/account';
import { ActivatedRoute } from '@angular/router';
import { Router, NavigationEnd } from '@angular/router';
import { ISearchResult } from './searchResult';

@Component({
  // selector: "search-results",
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {
  uuid = '';
  private fragment: string;
  public chosenAccount: ISearchResult;
  public chosenVin: string;
  // private chosenAccount: IAccount [];
  // private chosenAccount: IAccount [] = [];

  // private chosenAccount: IAccount;
  constructor(private route: ActivatedRoute,
  private router: Router) {


    router.events.subscribe(s => {
      if (s instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector("#" + tree.fragment);
          if (element) { element.scrollIntoView(true);
            window.scrollBy(0, -60);}
        }
      }
    });
  }

  showContactDetailsEditModal(account: ISearchResult): void {

    this.chosenAccount = account;

    console.log(this.chosenAccount);
    $('#contactDetailsEditModal').modal('show');

  }

  showVinDetailsEditModal(vin: string): void {

    this.chosenVin = vin;

    console.log('Chosen VIN: ', this.chosenVin);
    $('#vinDetailsEditModal').modal('show');

  }

  closeAccountModalClicked(id: string): void {

    // https://accounts-development.apps.pcfepgwi.gm.com/swagger-ui.html#!/accounts-controller/closeAccountUsingPUT

    $('#closeAccountModal').modal('show');

  }

  removeProfilesFromVin(vin: string): void {

    console.log('Removing all profiles from: ', vin);

  }

  deleteProfileFromAccount(id: string){

    console.log('removing profile:', id);


  }

  showFavoritesEditModal (account: ISearchResult): void {

    this.chosenAccount = account;

    console.log(this.chosenAccount);
    $('#favoritesEditModal').modal('show');



  }


ngOnInit() {

    const $navBar = $('#stickyNav');
    const navPos = $navBar.offset().top;

  (<any>$('#stickyNav')).affix({

    offset: {
      top: navPos
    }
  });

  this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
    this.route.params.subscribe( params => {console.log(params);
    this.uuid = params.uuid;});


  //hit microservice with UUID and get back info.
  //example response from said service:

    this.chosenAccount  = {
      contactInfo : {firstName : "Joseph", lastName : "Kanaan", userType: "Standard User", userName: "joseph.kanaan@gm.com" , recordType:"Info 3.0", email:"joseph.kanaan@gm.com", phone: "727-255-4198", info3AccountStatus: "Open", dob:"09/08/1990", id: "123"},
      vehicleProfiles : [{ yearMakeModel:"2018 Chevrolet Corvette", vehicleProfile : "Joseph's 2018 Corvette 1GNKVHKD9HJ302182", vehicleName : "1GNKVHKD9HJ302182", associationStatus: "Active", removeRequestDate: "N/A", vin: "1GNSCCKC7GR216661"},
        { yearMakeModel:"2018 Cadillac Escalade", vehicleProfile : "Joseph's 2018 Escalade AKHGYEFME1SJJG323", vehicleName : "AKHGYEFME1SJJG323", associationStatus: "Active", removeRequestDate: "N/A", vin: "1GNSCCKC7GR216661"}],
      applications: [{contactApplicationName: "CA-0451163", applicationName: "iHeartRadio", category: "Entertainment"}, {contactApplicationName: "CA-0451165", applicationName: "Spotify", category: "Music"}],
      agreements: [{agreementRefreshDate: "1/1/2018", agreementsAccepted: "Yes", status:"", lastAcceptDate: "1/1/2018"}],
      favorites: [{favoriteName: "Home", recordType:"Navigation", value: "1908 Montana Sky Dr"}, {favoriteName: "Work", recordType:"Navigation", value: "13201 McCallen Pass"}]

};

console.log("Showing Account Information for:" , this.chosenAccount.contactInfo.firstName);
console.log("With UUID:" , this.uuid);



}



}
