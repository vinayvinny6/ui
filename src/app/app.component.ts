import { Component } from '@angular/core';
// import { AccountSearchComponent } from 'search/account-search.component';
import {AccountService} from "./search/account.service";

@Component({
  selector: 'app-root',
  // templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  template:
  `<nav class="navbar navbar-default navbar1 custom" id="logoBanner">
    <div class="container-fluid navbar1">
      <div class="navbar-header">
        <a href="#" class="navbar-left"><img style="margin-right: 15px;" src="../assets/images/gm-brandmark.svg" height="50" width="50"></a>
        <a href="/accountsearch" style="color: black;"class="navbar-brand custom">Infotainment Advisor Hub</a>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <li>

          <div class="c-user-menu" aria-label="User Menu">
            <button class=" c-circle-icon-container" aria-controls="user-menu" title="user-menu">
              <i class="c-circle-icon fa fa-user"></i>  </button>
            <div class="c-dropdown">
              <a class="j-dropdown" aria-expanded="false">Ron Advisor</a>
              <ul class="c-dropdown-body" role="group">
                <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>
                <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a></li>
                <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Sign Out</a></li>
              </ul>
            </div>
          </div>
          
        </li>
      </ul>
      
    </div>
  </nav>
    <router-outlet></router-outlet>
  `,
  providers: [AccountService]
})
export class AppComponent {
  title = 'PIX Advisor UI';
}
