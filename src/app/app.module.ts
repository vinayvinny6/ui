import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import * as $ from 'jquery';
import * as bootstrap from 'bootstrap';

import { AppComponent } from './app.component';
import { AccountSearchComponent } from './search/account-search.component';
import { SearchResultsComponent } from './account/search-results.component';
import { StickyNavComponent } from './home/sticky-nav.component';
import { VehicleProfileComponent } from './vehicleprofile/vehicleprofile.component';


@NgModule({
  declarations: [
    AppComponent,
    AccountSearchComponent,
    SearchResultsComponent,
    StickyNavComponent,
    VehicleProfileComponent
  ],
  entryComponents:[AccountSearchComponent, SearchResultsComponent, AppComponent, StickyNavComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: 'accountsearch', component: AccountSearchComponent},
      {path: 'accountsearch/:uuid', component: SearchResultsComponent},
      {path: 'vehicleprofile/:vin', component: VehicleProfileComponent},
      {path: '', redirectTo: 'accountsearch', pathMatch: 'full'}
      // {path: '**', redirectTo: 'accountsearch', pathMatch: 'full'},


       // {path: '', redirectTo: 'SearchResultsComponent', pathMatch: "full"}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
