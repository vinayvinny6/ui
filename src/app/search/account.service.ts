import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';


import { IAccount } from './account';

@Injectable()
export class AccountService {

  private _accountServiceUrl = 'https://accounts-development.apps.pcfepgwi.gm.com/accounts?email=zac.grantham%40gm.com';

  //must add httpClient constructor and declare URL like above
  constructor(private _http: HttpClient) {}


  //calls getProducts and registers the result as an Observable object, mapped to the IProduct class. ProductList component must subscribe to this observable to get its changes/results
  getAccounts(): Observable<IAccount[]> {
    return this._http.get<IAccount[]>(this._accountServiceUrl)
      .do(data => console.log('All: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private handleError(err: HttpErrorResponse){
    console.log(err.message);
    return Observable.throw(err.message);
  }

}
