export interface IAccount {
  firstName: string;
  lastName: string;
  userType: string;
  yearMakeModel: string;
  vin: string;
  vehicleProfile: string;
  associationStatus: string;
  primaryEmail: string;
  userName: string,
  homePhone: string,
  recordType: string,
  zip: string;
  id: string;
}

// contactName: "Joseph Kanaan",
//   userType: "Standard User",
//   yearMakeModel: "2018 Chevrolet Corvette",
//   vin: "1GNKVHKD9HJ302182",
//   vehicleProfile: "Joseph's 2018 Corvette 1GNKVHKD9HJ302182",
//   associationStatus: "Active",
//   primaryEmail: "joseph.kanaan@gm.com",
//   userName: "joseph.kanaan@gm.com",
//   recordType: "Info 3.0",
//   homePhone: "727-867-5309",
//   zip: "78727"

// IAccount = {
//   contactInfo : [] = [{"contactName": string}, "yearMakeModel" : stringify];
//
// vehicleProfiles : [];
//
// }



