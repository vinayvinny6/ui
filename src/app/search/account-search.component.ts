import { Component, OnInit } from '@angular/core';
import { IAccount } from './account';
// import { AccountService } from './account.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SearchResultsComponent } from '../account/search-results.component';

@Component({
  // selector: "account-search",
  templateUrl: './account-search.component.html',
  styleUrls: ['./account-search.component.css']
})
export class AccountSearchComponent implements OnInit {

  returnedAccounts: any[] = new Array();
  emailInput: string;
  phoneInput: string;
  lastNameInput: string;
  firstNameInput: string;
  zipInput: string;
  vinInput: string;
  // returnedAccounts: IAccount[] = [];
  selectedAccount: IAccount = {  firstName: '',
    lastName: '',
  userType: '',
  yearMakeModel: '',
  vin: '',
  vehicleProfile: '',
  associationStatus: '',
  primaryEmail: '',
  userName: '',
  homePhone: '',
  recordType: '',
  zip: '',
  id: ''};

  // selectedAccount: IAccount = {};

  checkboxFlag: boolean = false;

  // private _accountService: AccountService,

  constructor(private router: Router, private http: HttpClient){}

  searchClicked(): void {

    $("#clearButtonDiv").show("slow");
    $('#searchDiv').css("padding-top", "20px");
    $("#resultsArea").show("slow");
    $("#searchDetails").show("slow");



  // this._accountService.getAccounts();


    this.searchByEmail();



    //TODO setup two way binding between inputs and ts file
    // console.log(this.emailInput);

     //subscribe to searchService onInit and get back array. hard code for now.
    //this._accountService.getAccounts().subscribe()...

    // this.returnedAccounts = [
    //   {
    //     contactName: "Joseph Kanaan",
    //     userType: "Standard User",
    //     yearMakeModel: "2018 Chevrolet Corvette",
    //     vin: "1GNKVHKD9HJ302182",
    //     vehicleProfile: "Joseph's 2018 Corvette 1GNKVHKD9HJ302182",
    //     associationStatus: "Active",
    //     primaryEmail: "joseph.kanaan@gm.com",
    //     userName: "joseph.kanaan@gm.com",
    //     recordType: "Info 3.0",
    //     homePhone: "727-255-4198",
    //     zip: "78727",
    //     uuid: "13579"
    //   },
    //   {
    //     contactName: "Hector Villarreal",
    //     userType: "Standard User",
    //     yearMakeModel: "2018 Cadillac Escalade",
    //     vin: "1GNKVHKD9HJ302182",
    //     vehicleProfile: "Hector's 2018 Escalade 1GNKVHKD9HJ302182",
    //     associationStatus: "Active",
    //     primaryEmail: "hector.villarreal@gm.com",
    //     userName: "hector.villarreal@gm.com",
    //     recordType: "Info 3.0",
    //     homePhone: "727-867-5310",
    //     zip: "78727",
    //     uuid: "246810"
    //   }
    // ];

  }

  searchByEmail(): void {

    this.http.get('https://accounts-development.apps.pcfepgwi.gm.com/accounts?email=zac.grantham%40gm.com').subscribe((data: any) => {

      console.log('response: ', data);

      this.returnedAccounts[0] = data.userDetails;
      this.returnedAccounts[0].id = data.id;

      console.log('data: ', this.returnedAccounts);


    });


  }

  showVerificationModal(account: IAccount): void {

    this.selectedAccount = account;

      console.log(this.selectedAccount.firstName);
      $('#myModal').modal('show');

  }

  verificationChecked() {

    this.checkboxFlag = !this.checkboxFlag;
    // document.getElementById('goToResultsButton').disabled = !this.checkboxFlag;

    (<HTMLInputElement> document.getElementById("goToResultsButton")).disabled = !this.checkboxFlag;

  }

  goToResults(account: IAccount): void {

    $('#myModal').modal('hide');

    console.log("Got it!");
    console.log(account);
    //redirect to new page using selectedAccount. unsure if i will have all info needed already at this point. TBD.

  }

  clearSearchClicked(): void{
    this.returnedAccounts = [];

    $('#searchDiv').css("padding-top", "120px");
    $("#clearButtonDiv").hide("slow");
    $("#searchArea").show("slow");
    $("#searchButton").show("slow");
    $("#searchDiv").show("slow");
    $("#resultsArea").hide("slow");
    $("#searchDetails").hide("slow");

  }

  ngOnInit(): void{
    console.log("in onInit");

    // document.getElementById('goToResultsButton').disabled = true;

    (<HTMLInputElement> document.getElementById("goToResultsButton")).disabled = true;


    // document.getElementsByTagName('body')[0].style = 'font-family: GM Global Sans';

    // $("#searchByPhoneInput").keyup(function(event) {
    //   if (event.keyCode === 13) {
    //     $("#searchButton").click();
    //   }
    // });


    const resultsArea = document.getElementById("resultsArea");
    const searchDetails = document.getElementById("searchDetails");
    const clearButton = document.getElementById("clearButtonDiv");
    clearButton.style.display = 'none';
    searchDetails.style.display = 'none';
    resultsArea.style.display = 'none';
  }
}
