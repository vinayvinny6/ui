import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Component({
  // selector: "account-search",
  templateUrl: './vehicleprofile.component.html',
  styleUrls: ['./vehicleprofile.component.css']
})
export class VehicleProfileComponent implements OnInit {


  returnedVehicle: any[] = new Array();
  vin: string;

  constructor (private route: ActivatedRoute, private http: HttpClient) {

  }


  ngOnInit() {


    // this.route.fragment.subscribe(fragment => { this.fragment = fragment;});
    this.route.params.subscribe( params => {console.log(params);
      this.vin = params.vin;});


    this.returnedVehicle = [{color: 'red', preferences: []}];

    this.getVehicleByVin(this.vin);


  }

  getVehicleByVin(vin: string): void {

    this.http.get('https://vehicles-development.apps.pcfepgwi.gm.com/vehicles/' + vin).subscribe(data => {


      this.returnedVehicle[0] = data;
      // this.returnedVehicle.push(data);
      console.log('returned vehicle: ', this.returnedVehicle);

    });

  }

}
